import java.io.IOException;

import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.ev3.EV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;
import lejos.utility.PilotProps;

public class OdometryCalibration {

	static EV3LargeRegulatedMotor leftMotor = new EV3LargeRegulatedMotor(MotorPort.A);
	static EV3LargeRegulatedMotor rightMotor = new EV3LargeRegulatedMotor(MotorPort.D);
	static EV3 ev3 = (EV3) BrickFinder.getDefault();

	static boolean isFinish = false;

	static MovePilot pilot;



	public static void main(String[] args) throws IOException{
		PilotProps pilotProps = new PilotProps();
		pilotProps.setProperty(PilotProps.KEY_WHEELDIAMETER, "5.366");
		pilotProps.setProperty(PilotProps.KEY_TRACKWIDTH, "12.1");
		pilotProps.setProperty(PilotProps.KEY_LEFTMOTOR, "A");
		pilotProps.setProperty(PilotProps.KEY_RIGHTMOTOR, "D");
		pilotProps.setProperty(PilotProps.KEY_REVERSE, "false");
		pilotProps.storePersistentValues();
		pilotProps.loadPersistentValues();
		float leftDiameter = Float.parseFloat("5.684") ;
		float rightDiameter = Float.parseFloat("5.650") ;
		float wheelDiameter = Float.parseFloat(pilotProps.getProperty(PilotProps.KEY_WHEELDIAMETER, "5.366"));
    	float trackWidth = Float.parseFloat(pilotProps.getProperty(PilotProps.KEY_TRACKWIDTH, "11.9"));
    	boolean reverse = Boolean.parseBoolean(pilotProps.getProperty(PilotProps.KEY_REVERSE, "false"));

    	Chassis chassis = new WheeledChassis(
    			new Wheel[]{
    					WheeledChassis.modelWheel(leftMotor,leftDiameter).offset(-trackWidth/2).invert(reverse),
    					WheeledChassis.modelWheel(rightMotor,rightDiameter).offset(trackWidth/2).invert(reverse)}, 
    			WheeledChassis.TYPE_DIFFERENTIAL);

    	pilot = new MovePilot(chassis);
    	pilot.setLinearSpeed(25);
    	pilot.setAngularSpeed(60);
    	pilot.stop();
    	int sag=90;
    	Button.waitForAnyPress();
    	int button=Button.readButtons();
    	if(button==Button.ID_ENTER) sag=-1*sag;

    	for(int i=0; i<4; i++){
    		pilot.travel(200,false);
        	pilot.rotate(sag);
    	}
    	/*
    	pilot.travel(200,false);
    	pilot.rotate(90);
    	pilot.travel(200,false);
    	pilot.rotate(90);
    	pilot.travel(200,false);
    	pilot.rotate(90);
*/

	}

}
